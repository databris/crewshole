package uk.ac.bris.rit.crewshole;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pldms
 */
public class DirectoryTarArchiverTest {
    
    final static Logger log = LoggerFactory.getLogger(DirectoryTarArchiverTest.class);
    
    public DirectoryTarArchiverTest() {
    }
    
    @Test
    public void testVanilla() throws IOException {
        Set<String> content = tarDir(Paths.get("src/test/resources/directories/one"), Paths.get("one"));
        
        assertThat(content, 
                is(set("one/", "one/IMG_1854.JPG", "one/IMG_1887.JPG", "one/IMG_1889.JPG")));
    }
    
    @Test
    public void testNoFollowSymlink() throws IOException {
        Set<String> content = tarDir(Paths.get("src/test/resources/directories/three"), Paths.get("three"));
        
        assertThat(content, 
                is(set("three/", "three/IMG_1887.JPG")));
    }
    
    @Test
    public void testNoIncludeZip() throws IOException {
        Set<String> content = tarDir(Paths.get("src/test/resources/directories/four"), Paths.get("four"));
        
        assertThat(content, 
                is(set("four/", "four/x.txt")));
    }
    
    @Test
    public void testVanillaZip() throws IOException {
        Set<String> content = zipDir(Paths.get("src/test/resources/directories/one"), Paths.get("one"));
        
        assertThat(content, 
                is(set("one/", "one/IMG_1854.JPG", "one/IMG_1887.JPG", "one/IMG_1889.JPG")));
    }
    
    @Test
    public void testNoFollowSymlinkZip() throws IOException {
        Set<String> content = zipDir(Paths.get("src/test/resources/directories/three"), Paths.get("three"));
        
        assertThat(content, 
                is(set("three/", "three/IMG_1887.JPG")));
    }
    
    @Test
    public void testNoIncludeZipZip() throws IOException {
        Set<String> content = zipDir(Paths.get("src/test/resources/directories/four"), Paths.get("four"));
        
        assertThat(content, 
                is(set("four/", "four/x.txt")));
    }
    
    @Test
    public void testTroubleCharZip() throws IOException {
        Set<String> content = zipDir(Paths.get("src/test/resources/directories/five"), Paths.get("five"));
        
        assertThat(content, 
                is(set("five/", "five/tricky–character")));
    }
    
    public Set<String> tarDir(final Path toCheck, final Path outRoot) throws IOException {
        
        InputStream in = pipe(new RunWithOutput() {
            @Override
            public void run() {
                
                TarArchiveOutputStream aOut = new TarArchiveOutputStream(this.out);
                
                // Tar away!
                try (DirectoryTarArchiver archiver
                        = new DirectoryTarArchiver(toCheck, outRoot,
                                aOut)) {
                    Files.walkFileTree(toCheck, archiver);
                } catch (IOException e) {
                    log.error("Problem walking file tree", e);
                }
            }
        });
        
        return listTarEntries(in);
    }

    public Set<String> zipDir(final Path toCheck, final Path outRoot) throws IOException {
        
        InputStream in = pipe(new RunWithOutput() {
            @Override
            public void run() {
                
                ZipArchiveOutputStream aOut = new ZipArchiveOutputStream(this.out);
                
                // Tar away!
                try (DirectoryTarArchiver archiver
                        = new DirectoryTarArchiver(toCheck, outRoot,
                                aOut)) {
                    Files.walkFileTree(toCheck, archiver);
                } catch (IOException e) {
                    log.error("Problem walking file tree", e);
                }
            }
        });
        
        return listZipEntries(in);
    }
    
    private Set<String> listTarEntries(InputStream in) throws IOException {
        Set<String> paths = new HashSet<>();

        TarArchiveInputStream tis = new TarArchiveInputStream(in);
        ArchiveEntry entry;

        while ((entry = tis.getNextEntry()) != null) {
            paths.add(entry.getName());
        }

        return paths;
    }

    private Set<String> listZipEntries(InputStream in) throws IOException {
        Set<String> paths = new HashSet<>();

        ZipArchiveInputStream tis = new ZipArchiveInputStream(in);
        ArchiveEntry entry;

        while ((entry = tis.getNextEntry()) != null) {
            paths.add(entry.getName());
        }

        return paths;
    }
    
    private Set<String> set(String... args) {
    	return new HashSet(Arrays.asList(args));
    }

    // Take output, pipe to input
    private static InputStream pipe(RunWithOutput runner) throws IOException {
        PipedInputStream in = new PipedInputStream();
        PipedOutputStream out = new PipedOutputStream(in);

        runner.setOutputStream(out);

        new Thread(runner).start();

        return in;
    }

    private static abstract class RunWithOutput implements Runnable {

        protected OutputStream out;

        public void setOutputStream(OutputStream out) {
            this.out = out;
        }

    }
}
