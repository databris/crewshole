package uk.ac.bris.rit.crewshole;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pldms
 */
public class TarServletTest {
    
    public TarServletTest() {
    }

    /**
     * Test of followSymLink method, of class TarServlet.
     */
    @Test
    public void testFollowSymLink() throws Exception {
        Path base = Paths.get("src/test/resources/directories");
        
        // Not a sym link
        assertThat(TarServlet.followSymLink(base.resolve("one")),
                is(base.resolve("one")));
        
        // Is a sym link
        assertThat(TarServlet.followSymLink(base.resolve("two")),
                is(Paths.get("src/test/resources/elsewhere/one")));
    }

    /**
     * Test of getRequestedPath method, of class TarServlet.
     */
    @Test
    public void testGetRequestedPath() throws Exception {
        
        Pattern allowed = Pattern.compile("[0-9a-f]+");
                
        // Strip off leading /
        assertThat(TarServlet.getRequestedPath("/abc", allowed),
                is("abc"));
        
        // Ignore tar
        assertThat(TarServlet.getRequestedPath("/abc.tar", allowed),
                is("abc"));
        
        // Bad, bad path
        assertThat(TarServlet.getRequestedPath("/bad/path", allowed),
                is(TarServlet.BAD_PATH));
        
        // Bad, bad path
        assertThat(TarServlet.getRequestedPath("/bad...", allowed),
                is(TarServlet.BAD_PATH));
    }
    
}
