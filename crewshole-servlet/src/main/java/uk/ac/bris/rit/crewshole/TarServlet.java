package uk.ac.bris.rit.crewshole;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Damian Steer <d.steer@bristol.ac.uk>
 */
public class TarServlet extends HttpServlet implements Filter {

    final static Logger log = LoggerFactory.getLogger(TarServlet.class);
    // Return value to indicate a bad path
    public final static String BAD_PATH = "this/is/a/bad/path/jeifoeioiioencwoicn"; 

    // Work out path from http request
    public static final String getRequestedPath(String pathInfo, Pattern allowed) throws IOException {
        // Remove leading '/' and '.tar' or '.zip' if present
        
        int lastIndex = pathInfo.endsWith(".tar") || pathInfo.endsWith(".zip") ? 
                pathInfo.length() - 4 : pathInfo.length();
        
        String requestedPath = pathInfo.substring(1, lastIndex);
        
        if (!allowed.matcher(requestedPath).matches()) {
            TarServlet.log.debug("Path <{}> does not match required '{}'", requestedPath, allowed.pattern());
            return BAD_PATH;
        }
        return requestedPath;
    }

    // Follow symlink if required
    public static final Path followSymLink(Path dir) throws IOException {
        Path actualDir;
        if (Files.isSymbolicLink(dir)) {
            Path target = Files.readSymbolicLink(dir);
            TarServlet.log.debug("Following top level sym link <{}> -> <{}>", dir, target);
            actualDir = dir.getParent().resolve(target).normalize();
        } else {
            actualDir = dir;
        }
        return actualDir;
    }
    
    private Path base;
    private Pattern allowedDirNameMatch;
    
    @Override
    public void init() throws ServletException {
        
        // Get the base directory from the init parameter
        String baseDir = getInitParameter("base");
        String allowedDirName = getInitParameter("allowed-directory-name");
        
        init(baseDir, allowedDirName);
        
    }
    
    private void init(String baseDir, String allowedDirName) throws ServletException {
        
        log.info("Base directory: <{}>", baseDir);
        log.info("Allowed directory name: '{}'", allowedDirName);
        
        if (baseDir == null) {
            throw new ServletException("No base init param provided");
        }
        
        base = Paths.get(baseDir);
        
        if (!Files.isDirectory(base) && !Files.isReadable(base)) {
            throw new ServletException("base init param must be a directory and readable: " + base);
        }
        
        allowedDirNameMatch = Pattern.compile(allowedDirName);
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // This is infuriating -- it depends on the web.xml 
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            pathInfo = request.getServletPath();
        }
        
        log.info("Archive raw path: {}", pathInfo);
        
        String outType;
        
        if (pathInfo.endsWith(".tar")) {
            outType = "application/x-tar"; 
        } else {
            outType = "application/zip";
        }
        
        // Work out path from servlet path -- strips tar and checks form against regex
        String requestedPath = getRequestedPath(pathInfo, allowedDirNameMatch);
        
        // getRequestedPath not happy, bail
        if (requestedPath == BAD_PATH) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Forbidden path");
            return;
        }
        
        // Resolve path relative to base
        Path dir = base.resolve(requestedPath);
        
        log.debug("Tar directory: {}", dir);
        
        // 404 if not accessible
        if (!Files.exists(dir) || !Files.isDirectory(dir)) {
            log.debug("<{}> not found or not a directory", dir);
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        
        String extension = outType.equals("application/x-tar") ? "tar" : "zip";
        
        // Good to go
        response.setStatus(HttpServletResponse.SC_OK);
        // Sending type
        response.setHeader("Content-Type", outType);
        // Normalise name for download
        response.setHeader("Content-Disposition", String.format("filename=\"%s.%s\"",
                requestedPath, extension));
        //response.setHeader("Last-Modified", Files.getLastModifiedTime(dir).toMillis());
        
        log.info("Sending directory <{}> via {} (requested <{}>)", 
                dir, extension, pathInfo);
        
        // We follow symlinks at this point _only_ (like -H on BSD tar)
        Path actualDir = followSymLink(dir);
        
        ArchiveOutputStream archiveOut;
        OutputStream out = response.getOutputStream();
        
        if (outType.equals("application/x-tar")) {
            TarArchiveOutputStream aOut = new TarArchiveOutputStream(out);
            // POSIX please
            aOut.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_POSIX);
            aOut.setLongFileMode(TarArchiveOutputStream.LONGFILE_POSIX);
            
            archiveOut = aOut;
        } else {
            ZipArchiveOutputStream aOut = new ZipArchiveOutputStream(out);
            aOut.setUseZip64(Zip64Mode.AsNeeded);
            aOut.setLevel(ZipEntry.STORED); // disable compression
            archiveOut = aOut;
        }
        
        // Tar away!
        try (DirectoryTarArchiver archiver = 
                new DirectoryTarArchiver(actualDir, dir.getFileName(),
                        archiveOut)) {
            Files.walkFileTree(actualDir, archiver);
        } catch (Exception e) {
            // This is almost always a client disconnect error, but there's no
            // standard way to detect that (Tomcat as ClientAbortException)
            log.warn(
                    String.format(
                            "Error archiving directory '%s': %s", 
                            dir, e.getMessage() ));
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Tars or zips directories on demand";
    }
    
    // Used when working as a filter
    
    @Override
    public void init(FilterConfig fc) throws ServletException {
        // Get the base directory from the init parameter
        String baseDir = fc.getInitParameter("base");
        String allowedDirName = fc.getInitParameter("allowed-directory-name");
        
        init(baseDir, allowedDirName);
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) sr;
        
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            pathInfo = request.getServletPath();
        }
        
        // Strip leading '/'
        Path file = base.resolve(pathInfo.substring(1));
        
        if ((pathInfo.endsWith(".tar") || pathInfo.endsWith(".zip"))
                && !Files.exists(file)) {
            log.info("Path ends with .tar or .zip, and there is no corresponding file. Archiving.");
            doGet(request, (HttpServletResponse) sr1);
        } else {
            fc.doFilter(sr, sr1);
        }
    }
    
}
