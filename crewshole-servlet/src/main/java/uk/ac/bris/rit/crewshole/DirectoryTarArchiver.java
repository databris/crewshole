package uk.ac.bris.rit.crewshole;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A directory tar tool
 *
 * @author Damian Steer <d.steer@bristol.ac.uk>
 */
public class DirectoryTarArchiver
        extends SimpleFileVisitor<Path>
        implements AutoCloseable {

    final static Logger log = LoggerFactory.getLogger(DirectoryTarArchiver.class);
    
    final static int bufSize = 4 * 1024;
    
    private final ArchiveOutputStream archiveOut;
    private final Path base;
    private final Path root;
    private final String rootName;
    private final byte[] buffer;

    /**
     * A file visitor that tars as it goes
     *
     * @param base Base path - all tar entries will be relative to this path,
     * under root
     * @param root This path will be used as the root in the archive
     * @param archiveOut ArchiveOutputStream stream
     */
    public DirectoryTarArchiver(Path base, Path root, ArchiveOutputStream archiveOut) {
        this.base = base;
        this.root = root;
        this.rootName = root.getFileName().toString();
        this.buffer = new byte[bufSize];
        
        this.archiveOut = archiveOut;
    }

    /**
     * Adds directory entry to tar
     *
     * @param file
     * @param attrs
     * @return
     * @throws IOException
     */
    @Override
    public FileVisitResult preVisitDirectory(Path file, BasicFileAttributes attrs) throws IOException {
        log.debug("Archive directory <{}>", file);

        // Relativise
        Path relativePath = base.relativize(file);

        // Add dir to tar
        ArchiveEntry entry = 
                archiveOut.createArchiveEntry(file.toFile(), root.resolve(relativePath).toString());
        
        archiveOut.putArchiveEntry(entry);
        archiveOut.closeArchiveEntry();
        return FileVisitResult.CONTINUE;
    }

    /**
     * Add file contents to tar
     *
     * @param file
     * @param attrs
     * @return
     * @throws IOException
     */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (!attrs.isRegularFile()) {
            log.debug("Skip irregular file <{}>", file);
            return FileVisitResult.CONTINUE;
        }
        
        // Exclude files like /xyz/xyz.zip
        if (file.getNameCount() > 1 && file.getFileName().toString().startsWith(rootName)) {
            log.debug("Skip file <{}> which starts with root name <{}>", file, rootName);
            return FileVisitResult.CONTINUE;
        }
        
        log.trace("Tar file <{}>", file);

        // Relativise
        Path relativePath = base.relativize(file);

        // Add dir to tar
        ArchiveEntry entry = 
                archiveOut.createArchiveEntry(file.toFile(), root.resolve(relativePath).toString());
        
        archiveOut.putArchiveEntry(entry);
        
        try (InputStream in = Files.newInputStream(file)) {
            IOUtils.copyLarge(in, archiveOut, buffer);
        }
        
        archiveOut.closeArchiveEntry();
        
        return FileVisitResult.CONTINUE;
    }
    
    @Override
    public void close() throws IOException {
        archiveOut.finish();
        archiveOut.flush();
        archiveOut.close();
    }

}
