# Welcome to Crewshole #

## Introduction ##

This is a simple web application which will zip or tar directories on the fly.

Note that no compression is applied, just packaging. In practice we create in advance zips of deposits below a certain threshold, and use this tool to distribute larger deposits where storing a zip would be expensive in terms of space.

## Prerequisites ##

Java 7 or higher, and the maven build tool.

## Configuration ##

The application needs two parameters (set in `web.xml`):

*`base`*: the base directory. Directories within this directory will packaged on demand. Crewshole will also follow symbolic links at the top level.

*`allowed-directory-name`*: a regular expression matched against the path within the base directory. `*` will allow everything.

## Quick start ##

    $ git clone https://bitbucket.org/databris/crewshole.git
    $ cd crewshole
    $ mvn install
    ...builds, hopefully...
    $ cd crewshole-war
    $ vi src/main/webapp/WEB-INF/web.xml

Edit the parameters `base` and `allowed-directory-name`.

    $ mvn tomcat:run

You should now be able to visit the url:

    http://localhost:8080/crewshole-war/a.zip

(or .tar) and you will download the zipped contents of `{base}/a`.

## Why the name? ##

It's an area of Bristol which had a tar works.

## Help? ##

Damian Steer [d.steer@bris.ac.uk](mailto:d.steer@bris.ac.uk).
